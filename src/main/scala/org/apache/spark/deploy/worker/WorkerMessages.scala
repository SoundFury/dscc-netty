package org.apache.spark.deploy.worker

/**
  * Created by Administrator on 2016/8/19 0019.
  */
sealed trait WorkerMessages extends Serializable

private[spark] object WorkerMessages {

  case class RegisterWorker(host: String, port: Int) extends WorkerMessages
}