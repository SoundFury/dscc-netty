package org.apache.spark.util

import org.apache.spark.rpc.RpcEnv

/**
  * Created by Administrator on 2016/8/18.
  */
class SparkEnv(private[spark] val rpcEnv: RpcEnv)

object SparkEnv {
  @volatile private var env: SparkEnv = _

  private[spark] val driverSystemName = "sparkDriver"
  private[spark] val executorSystemName = "sparkExecutor"

  def set(e: SparkEnv) {
    env = e
  }

  /**
    * Returns the SparkEnv.
    */
  def get: SparkEnv = {
    env
  }

}
