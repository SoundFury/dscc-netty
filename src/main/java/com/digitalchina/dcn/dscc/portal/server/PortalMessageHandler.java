package com.digitalchina.dcn.dscc.portal.server;

import com.digitalchina.dcn.dscc.portal.protocol.PortalMessage;

import com.digitalchina.dcn.dscc.portal.protocol.PortalMessage.*;
/**
 * Created by Administrator on 2016/8/23 0023.
 */
public interface PortalMessageHandler {

    /** Handles the receipt of a single message. */
    void handle(PortalMessage message) throws Exception;

    /** Invoked when the channel this MessageHandler is on is active. */
    void channelActive();

    /** Invoked when an exception was caught on the Channel. */
    void exceptionCaught(Throwable cause);

    /** Invoked when the channel this MessageHandler is on is inactive. */
    void channelInactive();

}
