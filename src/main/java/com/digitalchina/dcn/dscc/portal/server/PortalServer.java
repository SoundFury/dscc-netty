package com.digitalchina.dcn.dscc.portal.server;

import com.digitalchina.dcn.dscc.portal.client.AuthInfo;
import com.digitalchina.dcn.dscc.portal.client.PortalClient;
import com.digitalchina.dcn.dscc.portal.protocol.PortalAttribute;
import com.digitalchina.dcn.dscc.portal.protocol.PortalMessage;
import com.digitalchina.dcn.dscc.portal.protocol.PortalMessageFactory;
import com.digitalchina.dcn.dscc.portal.util.CHAP;
import com.digitalchina.dcn.dscc.portal.util.PortalContext;
import io.netty.bootstrap.Bootstrap;
import io.netty.buffer.PooledByteBufAllocator;
import io.netty.channel.ChannelFuture;
import io.netty.channel.ChannelInitializer;
import io.netty.channel.ChannelOption;
import io.netty.channel.EventLoopGroup;
import io.netty.channel.socket.DatagramChannel;
import org.apache.commons.collections.functors.ExceptionClosure;
import org.apache.spark.network.util.IOMode;
import org.apache.spark.network.util.JavaUtils;
import org.apache.spark.network.util.NettyUtils;
import org.apache.spark.network.util.TransportConf;
import org.apache.spark.util.ThreadUtils;
import org.eclipse.jetty.util.MultiException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.Closeable;
import java.io.IOException;
import java.net.BindException;
import java.net.InetSocketAddress;
import java.nio.charset.StandardCharsets;
import java.security.NoSuchAlgorithmException;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

import com.digitalchina.dcn.dscc.portal.protocol.PortalMessage.*;

/**
 * Created by Administrator on 2016/8/21.
 */
public class PortalServer implements Closeable, PortalMessageHandler, MessageSendCallback{

    private final Logger logger = LoggerFactory.getLogger(PortalServer.class);

    private final PortalContext context;
    private final TransportConf conf;
    private PortalClient client;

    private Bootstrap bootstrap;
    private ChannelFuture channelFuture;
    // 监听AC发送的NTF_LOGOUT消息 端口
    // 用户强制下线消息
    private int ntfLogoutPort = PortalContext.NTF_LOGOUT_PORT;
    /** 向AC UDP 2000发送portal消息的本地UDP端口  */
    private int port = -1;

    /** 认证类型，目前支持CHAP、PAP */
    private final String authType;
    /** 消息发送后，未收到响应的超时时间 */
    private final int msgTimeout;
    /** 每个用户认证流程的超时时间 */
    private final int authTimeout;
    /** key为用户IP地址，<br/>
     * AuthKey为用户一次认证过程中的唯一标识，
     * 包括portal server生成的serialNo 和 AC生成的reqID <br/>
     * 认证完成后，删除*/
    private final ConcurrentHashMap<UserIP, AuthInfo> userAuthKeys = new ConcurrentHashMap<>();
    /** 记录用户当前认证状态<br/>
     * 认证完成后，删除 */
    private final ConcurrentHashMap<UserIP, AuthStatus> userStatus = new ConcurrentHashMap<>();

    /** AC portal协议监听地址 */
    private static InetSocketAddress acAddress;
    /** 超时定时任务服务 */
    private ScheduledExecutorService timeoutScheduler =
            ThreadUtils.newDaemonSingleThreadScheduledExecutor("portal-server-timeout");

    public PortalServer(PortalContext context,
                        String hostToBind,
                        int portToBind) {
        this.context = context;
//        this.appRpcHandler = appRpcHandler;
        this.conf = context.getConf();

        // 读取用户配置的接受用户异常下线消息
        // 监听端口
        ntfLogoutPort = context.ntfLogoutPort();
        // 获取认证类型，CHAP 或者 PAP
        authType = context.authType();
        // 获取portal 消息响应超时时间
        msgTimeout = context.msgTimeoutInMill();
        authTimeout = context.authTimeoutInMill();
        acAddress = new InetSocketAddress("192.168.3.100", 2000);
        try {
            init(hostToBind, portToBind);
        } catch (RuntimeException e) {
            JavaUtils.closeQuietly(this);
            throw e;
        }
    }

    public int getPort() {
        if (port == -1) {
            throw new IllegalStateException("Server not initialized");
        }
        return port;
    }

    private void init(String hostToBind, int portToBind) {

        initPortalClient();
    }

    private void initPortalClient() {
        IOMode ioMode = IOMode.valueOf(conf.ioMode());
        EventLoopGroup bossGroup =
                NettyUtils.createEventLoop(ioMode, conf.serverThreads(), "portal-client");

        PooledByteBufAllocator allocator = NettyUtils.createPooledByteBufAllocator(
                conf.preferDirectBufs(), true /* allowCache */, conf.serverThreads());

        bootstrap = new Bootstrap()
                .group(bossGroup)
                .channel(NettyUtils.getDatagramChannelClass(ioMode))
                .option(ChannelOption.ALLOCATOR, allocator);

        if (conf.backLog() > 0) {
            bootstrap.option(ChannelOption.SO_BACKLOG, conf.backLog());
        }

        if (conf.receiveBuf() > 0) {
            bootstrap.option(ChannelOption.SO_RCVBUF, conf.receiveBuf());
        }

        if (conf.sendBuf() > 0) {
            bootstrap.option(ChannelOption.SO_SNDBUF, conf.sendBuf());
        }

        bootstrap.handler(new ChannelInitializer<DatagramChannel>() {
            @Override
            protected void initChannel(DatagramChannel ch) throws Exception {
                context.initializePipeline(ch, PortalServer.this);
                client = new PortalClient(ch);
            }
        });

//        channelFuture = bootstrap.connect(acAddress);
        channelFuture = startServiceOnPort(2000, bootstrap);

        port = ((InetSocketAddress) channelFuture.channel().localAddress()).getPort();
        logger.debug("connected to AC on port: " + port);
    }

    @Override
    public void close() throws IOException {
        if (channelFuture != null) {
            // close is a local operation and should finish within milliseconds; timeout just to be safe
            channelFuture.channel().close().awaitUninterruptibly(10, TimeUnit.SECONDS);
            channelFuture = null;
        }
        if (bootstrap != null && bootstrap.group() != null) {
            bootstrap.group().shutdownGracefully();
        }
        bootstrap = null;

        client.close();
        client = null;
    }

    public static InetSocketAddress getAcAddress() {
        return acAddress;
    }

    @Override
    public void handle(PortalMessage message) throws Exception {
        Type msgType = message.type();

        AuthStatus receive = AuthStatus.getStatusByPortalMessage(message);
        AuthStatus current = userStatus.get(message.userIP());

        if (current == null) {
            // 如果该用户认证流程已被终止，
            logger.warn("{}'s auth has been terminated!", message.userIP());
            return;
        }
        if (receive.getStatus() <= current.getStatus()) {
            // receive报文是超时重发的报文，已经接受处理过，则丢弃
            logger.warn("{}'s current status is {}, but received status is !",
                    message.userIP(), current, receive);
            return;
        } else {
            // 更新 用户认证状态
            userStatus.put(message.userIP(), receive);
        }

        AuthInfo key = userAuthKeys.get(message.userIP());
        if(key == null || key.serialNo != message.serialNumber()) {
            logger.warn("{}'s serialNo is {}, but received message's serialNo is {}"
                    , message.userIP(), key.serialNo, message.serialNumber());
            return;
        }

        switch (msgType) {
            case ChallengeAck:
                processChallengeAck(message);
                break;
            case AuthAck:
                processAuthAck(message);
                break;
            case LogoutAck:
                processLogoutAck(message);
                break;
            default:
                logger.warn("error portal message type from ac! type = " + msgType.name());
        }

    }

    /**
     * 用户下线请求响应 消息处理
     * @param message
     */
    private void processLogoutAck(PortalMessage message) {
        UserIP userIP = message.userIP();
        ErrorCode result = message.errorCode();

        AuthInfo info = userAuthKeys.get(userIP);
        switch (result) {
            case LogoutSuccess:
                logger.info("{} logout successful in {} mill-seconds"
                        , userIP, System.currentTimeMillis() - info.startTime);

                break;
            case LogoutReject:
                logger.warn("{}'s logout request has been reject", userIP);

                break;
            case LogoutFailure:
                logger.warn("{}'s logout request has failure", userIP);

                break;
            default:
                logger.warn("unsupported result code {} of " +
                        "logout ack message for {}", result, userIP);
        }

        removeUserAuth(message.userIP());
    }

    private void processAuthAck(PortalMessage message) {
        UserIP userIP = message.userIP();
        ErrorCode resultCode = message.errorCode();

        AuthInfo info = userAuthKeys.get(userIP);
        switch (resultCode) {
            case AuthSuccess:
                // 认证成功
                logger.info("{} has been auth successful in {} mill-seconds!",
                        userIP, System.currentTimeMillis() - info.startTime);
                removeUserAuth(userIP);
                break;
            case AuthExist:
                // 用户已认证
                logger.warn("{} has been auth already!");
                removeUserAuth(userIP);
                break;
            case AuthReject:
            case AuthBusy:
            case AuthFailure:
                // 拒绝、忙、错误，
                // 都按照 认证失败处理
                removeUserAuth(userIP);
                logger.warn("{} auth failure, the result code is {} !", userIP, resultCode);

                break;
            default:
                logger.error("unsupported result code in AuthAck message: {}", resultCode);
                logger.trace(message.toString());
                removeUserAuth(userIP);
        }
    }

    /**
     *  删除用户认证信息
     * @param userIP
     */
    private void removeUserAuth(UserIP userIP) {
        userAuthKeys.remove(userIP);
        userStatus.remove(userIP);
    }

    private void processChallengeAck(PortalMessage message) {
        UserIP userIP = message.userIP();
        AuthInfo info = userAuthKeys.get(userIP);
        info.reqID = message.requestID();

        ErrorCode resultCode = message.errorCode();
        switch (resultCode) {
            case ChallengeSuccess:
                // 获取Challenge成功
                logger.info("{} get challenge successful in {} mill-seconds!",
                        userIP, System.currentTimeMillis() - info.startTime);
                PortalAttribute challengeAttr = message.attributes().get(0);
                if (challengeAttr.getType() != PortalAttribute.Type.Challenge) {
                    logger.warn("the challenge ack message's attribute type isn't " +
                            "challenge attribute! attribute type = " + challengeAttr.getType());
                    logger.trace(message.toString());

                    removeUserAuth(message.userIP());
                    return;
                }

                byte chapID = (byte) (message.requestID() & 0xff);
                byte[] challenge = challengeAttr.getValue();
                byte[] password = "test".getBytes(StandardCharsets.UTF_8);
                try {
                    byte[] chapPassword = CHAP.chapMD5(chapID, password, challenge);
                    // 发送认证请求报文
                    PortalMessage msg = PortalMessageFactory.createCHAPAuthRequestMessage(message.userIP(),
                            message.serialNumber(), message.requestID(), info.userName, chapPassword);
                    client.send(msg, this);
                } catch (NoSuchAlgorithmException e) {
                    logger.warn("doesn't support md5 algorithm!", e);
                    removeUserAuth(message.userIP());
                    return;
                }
                break;
            case ChallengeExist:Exist:
                // 用户Challenge已存在
                logger.warn("{} has got challenge already!", userIP);
                removeUserAuth(userIP);
                break;
            case ChallengeReject:
            case ChallengeBusy:
            case ChallengeFailure:Failure:
                // 拒绝、忙、错误，
                // 都按照 认证失败处理
                removeUserAuth(userIP);
                logger.warn("{} get challenge failure, the result code is {} !", userIP, resultCode);

                break;
            default:
                logger.error("unsupported result code in ChallengeAck message: {}", resultCode);
                logger.trace(message.toString());
                removeUserAuth(userIP);
        }
    }

    @Override
    public void channelActive() {
    }

    @Override
    public void exceptionCaught(Throwable cause) {

    }

    /**
     * 和AC的通道关闭后，需要重新建立一条通道
     */
    @Override
    public void channelInactive() {
        client.close();
        bootstrap.group().shutdownGracefully();
        initPortalClient();
    }

    /**
     * 发送potal消息失败后，需要清理该用户的认证信息，
     * 并向用户返回认证失败及原因
     * @param userIP
     * @param msgType
     * @param e
     */
    @Override
    public void onFailure(PortalMessage.UserIP userIP, PortalMessage.Type msgType, Throwable e) {
        logger.warn("send {}'s {} message fail!", userIP, msgType);
        logger.warn("error cause:", e);

        // 删除该用户的认证信息
        removeUserAuth(userIP);
    }


    /**
     * 报文发送成功后，需要启动接受相应报文超时机制
     * @param msg
     */
    @Override
    public void onSuccess(final PortalMessage msg) {
        if (isTimeoutMessage(msg)){
            return;
        }

        logger.info("send portal message {} successful " +
                "for {}", msg.type(), msg.userIP());

        // 写入serialNo
        AuthInfo key = userAuthKeys.get(msg.userIP());
        if (key == null) {
            // key为空，说明该用户的认证流程已被终止
            logger.warn("{}'s serialNo and reqID is null", msg.userIP());
            return;
        }
            userAuthKeys.put(msg.userIP(), key);
        // 更新用户认证状态
        if (userStatus.get(msg.userIP()) == null) {
            // status为空，说明该用户的认证流程已被终止
            logger.warn("{}'s status is null", msg.userIP());
            return;
        }
        final AuthStatus status = AuthStatus.getStatusByPortalMessage(msg);
        userStatus.put(msg.userIP(), status);

        PortalMessage timeoutMessage = null;
        if (msg.type() == Type.AuthReq) {
            // 认证请求超时消息的serialNo, reqID
            // 和 请求消息保持一致
            timeoutMessage = PortalMessageFactory
                    .createTimeoutMessage(msg.authType(), msg.serialNumber()
                            , msg.requestID(), msg.userIP());
        } else if (msg.type() == Type.ChallengeReq ||
                msg.type() == Type.LogoutReq) {
            // 保持serialNo, reqID写0
            timeoutMessage = PortalMessageFactory
                    .createTimeoutMessage(msg.authType(), msg.serialNumber(), 0, msg.userIP());
        }

        final PortalMessage message = timeoutMessage;
        if (timeoutMessage != null) {
            timeoutScheduler.schedule(new Runnable() {
                @Override
                public void run() {
                    AuthStatus currentStatus = userStatus.get(msg.userIP());
                    if (currentStatus != null && currentStatus == status) {
                        logger.warn("have not received the response message " +
                                "in {} mill-seconds for {} message about {}", msgTimeout, msg.type(), msg.userIP());
                        client.send(message, PortalServer.this);
                    }
                }
            }, msgTimeout, TimeUnit.MILLISECONDS);
        }
    }

    private boolean isTimeoutMessage(PortalMessage msg) {
        if (msg.type() == Type.LogoutReq && msg.errorCode() == ErrorCode.Timeout) {
            return true;
        }

        return false;
    }

    /**
     * 开始用户下线
     * @param userIP
     */
    public void startUserLogout(String userIP) {
//        // 判断用户是否在线
//        // 不在线，则不处理
//        if (!isUserOnline(userIP)) {
//            logger.warn("{} isn't online now," +
//                    " can't start logout process!", userIP);
//            return;
//        }

        // 写入用户下线请求状态
        final UserIP user = new UserIP(userIP);
        userStatus.put(user, AuthStatus.LogoutReq);

        PortalMessage send = null;
        if (authType.equalsIgnoreCase(PortalContext.AUTH_TYPE_CHAP)) {
            // CHAP认证
            send = PortalMessageFactory.createLogoutRequestMessage(AuthType.CHAP, user);
        } else {
            // PAP认证
            send = PortalMessageFactory.createLogoutRequestMessage(AuthType.PAP, user);
        }

        // 写入用户认证Key
        AuthInfo key = new AuthInfo(null, send.serialNumber());
        userAuthKeys.put(user, key);

        client.send(send, this);
        // 开启认证超时定时任务
        timeoutScheduler.schedule(new Runnable() {
            @Override
            public void run() {
                AuthStatus currentStatus = userStatus.get(user);
                if (currentStatus != null && currentStatus.getStatus()
                        < AuthStatus.LogoutAck.getStatus()) {
                    logger.warn("have not get the logout result " +
                            "in {} mill-seconds for {}, current status is {}"
                            , authTimeout,  user, currentStatus);
                    removeUserAuth(user);
                }
            }
        }, authTimeout, TimeUnit.MILLISECONDS);

        logger.info("starting logout for {}", userIP);
    }

//    /**
//     * 用户是否已在线
//     * @param userIP
//     * @return
//     */
//    private boolean isUserOnline(String userIP) {
//        return false;
//    }

    /**
     * 开始用户认证
     * @param userIP
     * @param userName
     * @param password
     */
    public void startUserAuth(String userIP, String userName, String password) {
        // 判断用户IP是否正在认证，
        // 只基于IP判断，同一用户在不同IP地址上,
        // 判断为不同的认证流程，可以开启认证
        if (isUserInAuthing(userIP)) {
            logger.warn("{} is already in auth process " +
                    "by user {}", userIP, userName);
            return;
        }
//        // 判断用户是否已在线
//        // 在线则不开启认证流程
//        if (isUserOnline(userIP)) {
//            logger.warn("{} is already online, can't auth again!", userIP);
//        }

        // 写入用户认证开始状态
        final UserIP user = new UserIP(userIP);
        userStatus.put(user, AuthStatus.Started);

        PortalMessage send = null;
        if (authType.equalsIgnoreCase(PortalContext.AUTH_TYPE_CHAP)) {
            // CHAP认证
            send = PortalMessageFactory.createChallengeMessage(userIP);
        } else {
            // PAP认证
            send = PortalMessageFactory.createPAPAuthRequestMessage(user, userName, password);
        }

        // 写入用户认证Key
        AuthInfo key = new AuthInfo(userName, send.serialNumber());
        userAuthKeys.put(user, key);

        client.send(send, this);
        // 开启认证超时定时任务
        timeoutScheduler.schedule(new Runnable() {
            @Override
            public void run() {
                AuthStatus currentStatus = userStatus.get(user);
                if (currentStatus != null && currentStatus.getStatus()
                        < AuthStatus.AuthAck.getStatus()) {
                    logger.warn("have not get the auth result " +
                            "in {} mill-seconds for {}, current status is {}"
                            , authTimeout,  user, currentStatus);
                    removeUserAuth(user);
                }
            }
        }, authTimeout, TimeUnit.MILLISECONDS);

        logger.info("starting auth for {}", userIP);
    }

    /**
     * 判断用户IP是否正在认证流程中
     * @param userIP
     * @return
     */
    private boolean isUserInAuthing(String userIP) {
        AuthInfo info = userAuthKeys.get(new UserIP(userIP));
        return info != null;
    }

    /**
     * Attempt to start a service on the given port, or fail after a number of attempts.
     * Each subsequent attempt uses 1 + the port used in the previous attempt (unless the port is 0).
     *
     * @param startPort The initial port to start the service on.
     */
    private ChannelFuture startServiceOnPort(
    int startPort,
    Bootstrap bootstrap) {
        // "startPort should be between 1024 and 65535 (inclusive), or 0 for a random free port."
        assert (startPort == 0 || (1024 <= startPort && startPort < 65536));

        int maxRetries = context.portMaxRetries();
        for (int offset = 0; offset < maxRetries; offset++) {
            // Do not increment port if startPort is 0, which is treated as a special port
            int tryPort;
            if (startPort == 0) {
                tryPort = 0;
            } else {
                // If the new port wraps around, do not try a privilege port
                tryPort = ((startPort + offset - 1024) % (65536 - 1024)) + 1024;
            }
            try {
                ChannelFuture future = bootstrap.bind(tryPort);
                future.syncUninterruptibly();
                logger.info("Successfully started service on port {}.", tryPort);
                return future;
            } catch(Exception e) {
                if (isBindCollision(e)) {
                    if (offset >= maxRetries) {
                        String exceptionMessage = e.getMessage() + ": Service failed after " +
                                maxRetries + " retries!";
                        BindException exception = new BindException(exceptionMessage);
                        // restore original stack trace
                        exception.setStackTrace(e.getStackTrace());
                        return bootstrap.bind(0);
                    }
                }
                logger.warn("Service could not bind on port {}. " +
                        "Attempting port {}.", tryPort, tryPort + 1);
            }
        }
        // Should never happen
        logger.error("Failed to start service$serviceString on port {}", startPort);
        return null;
    }

    /**
     * Return whether the exception is caused by an address-port collision when binding.
     */
    private boolean isBindCollision(Throwable exception) {
        if (exception instanceof BindException) {
            if (exception.getMessage() != null) {
                return true;
            }

            return isBindCollision(exception.getCause());
        } else if (exception instanceof MultiException) {
            MultiException me = (MultiException) exception;
            for (Throwable e : me.getThrowables()) {
                if (isBindCollision((e))) {
                    return true;
                }
            }
            return false;
        } else if (exception instanceof Exception) {
            return isBindCollision(exception.getCause());
        }

        return false;
    }
}
