package com.digitalchina.dcn.dscc.portal.server;

import com.digitalchina.dcn.dscc.portal.protocol.PortalMessage;

/**
 * Created by Administrator on 2016/8/24 0024.
 */
public interface MessageSendCallback {
    /** Invoked when send a message to AC failure */
    void onFailure(PortalMessage.UserIP userIP, PortalMessage.Type msgType, Throwable e);

    /** Invoked when send a message to AC success */
    void onSuccess(PortalMessage msg);
}
