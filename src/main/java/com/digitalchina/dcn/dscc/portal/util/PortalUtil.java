package com.digitalchina.dcn.dscc.portal.util;

import io.netty.channel.Channel;

import java.util.Random;

/**
 * Created by Administrator on 2016/8/23 0023.
 */
public class PortalUtil {

    private static String hexString = "0123456789ABCDEF";
    private static Random random = new Random();

    /**
     * 随机生成2个字节长度的SN号
     * @return
     */
    public static int getNextSerailNumber() {
        StringBuilder builder = new StringBuilder();
        for (int i = 0; i < 4; i ++) {
            int index = random.nextInt(16);
            builder.append(hexString.charAt(index));
        }
        return Integer.parseInt(builder.toString(), 16);
    }

}
