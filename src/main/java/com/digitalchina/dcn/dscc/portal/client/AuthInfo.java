package com.digitalchina.dcn.dscc.portal.client;

/**
 * Portal Server和AC一次认证过程的唯一标识
 *
 * Created by Administrator on 2016/8/23 0023.
 */
public class AuthInfo {

    public final long startTime = System.currentTimeMillis();

    public final String userName;
    /**
     * Portal Server生成的SN号
     */
    public final int serialNo;
    /**
     * AC生成的 reqID;
     */
    public int reqID;

    public AuthInfo(String userName, int serialNo) {
        this.userName = userName;
        this.serialNo = serialNo;
    }
}
