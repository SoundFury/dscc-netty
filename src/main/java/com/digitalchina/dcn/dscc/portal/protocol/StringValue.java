package com.digitalchina.dcn.dscc.portal.protocol;

import io.netty.buffer.ByteBuf;

import java.nio.charset.StandardCharsets;

/**
 * Created by Administrator on 2016/8/25 0025.
 */
public class StringValue extends OctetsValue {


    public StringValue(String value) {
        byteValue = value.getBytes(StandardCharsets.UTF_8);
    }

    @Override
    public String toString() {
        return new String(byteValue, StandardCharsets.UTF_8);
    }
}
