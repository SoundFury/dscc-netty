package com.digitalchina.dcn.dscc.portal.protocol;

import com.digitalchina.dcn.dscc.portal.server.PortalServer;
import io.netty.buffer.ByteBuf;
import io.netty.channel.ChannelHandler;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.socket.DatagramPacket;
import io.netty.handler.codec.MessageToMessageEncoder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.net.InetSocketAddress;
import java.util.List;

/**
 * Created by Administrator on 2016/8/21.
 */
@ChannelHandler.Sharable
public final class PortalMessageEncoder extends MessageToMessageEncoder<PortalMessage> {

//    private final Logger logger = LoggerFactory.getLogger(PortalMessageEncoder.class);

    //    private InetSocketAddress acAddress = PortalServer.getAcAddress();
    private InetSocketAddress acAddress = new InetSocketAddress("192.168.3.100", 2000);

    @Override
    protected void encode(ChannelHandlerContext ctx, PortalMessage in, List<Object> out) {
        int headerLength = in.encodedLength();
        ByteBuf buf = ctx.alloc().heapBuffer(headerLength);
        in.encode(buf);
        assert buf.writableBytes() == 0;

        out.add(new DatagramPacket(buf, acAddress));
    }
}
