package com.digitalchina.dcn.dscc.portal.protocol;

import io.netty.buffer.ByteBuf;
import org.apache.commons.codec.binary.Hex;

/**
 * Created by Administrator on 2016/8/25 0025.
 */
public class OctetsValue implements AttributeValue{

    protected byte[] byteValue;

    public OctetsValue() {}

    public OctetsValue(byte[] value) {
        this.byteValue = value;
    }

    @Override
    public String toString() {
        if (byteValue == null) {

        } else {
            return "0x" + Hex.encodeHexString(byteValue);
        }
        return "[Binary Data (length=0)]";
    }

    @Override
    public byte[] getBytes() {
        return byteValue;
    }

    @Override
    public int encodedLength() {
        return byteValue.length;
    }

    @Override
    public void encode(ByteBuf buf) {
        buf.writeBytes(byteValue);
    }
}
