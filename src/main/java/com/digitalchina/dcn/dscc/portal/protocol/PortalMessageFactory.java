package com.digitalchina.dcn.dscc.portal.protocol;

import com.digitalchina.dcn.dscc.portal.util.PortalUtil;

import com.digitalchina.dcn.dscc.portal.protocol.PortalMessage.*;
/**
 * Created by Administrator on 2016/8/23 0023.
 */
public class PortalMessageFactory {

    public static final String CHAP = "chap";
    public static final String PAP = "pap";
    public static final int DEFAULT_REQ_ID = 0;

    /**
     * 创建Challenge请求消息
     * @param userIP
     * @return
     */
    public static PortalMessage createChallengeMessage(String userIP) {
        int sn = PortalUtil.getNextSerailNumber();
        UserIP ip = new UserIP(userIP);

        return new PortalMessage(Type.ChallengeReq, AuthType.CHAP, sn, DEFAULT_REQ_ID, ip, ErrorCode.Default);
    }

    /**
     * 生成 PAP认证方式的 认证请求消息
     * @param userIP
     * @param username
     * @param password
     * @return
     */
    public static PortalMessage createPAPAuthRequestMessage(UserIP userIP
            , String username, String password) {
        int serialNo = PortalUtil.getNextSerailNumber();

        PortalMessage message = new PortalMessage(Type.AuthReq, AuthType.PAP, serialNo, DEFAULT_REQ_ID, userIP, ErrorCode.Default);
        message.addAttribute(PortalAttributeFacotry.createUsernameAttribute(username));
        message.addAttribute(PortalAttributeFacotry.createPasswordAttribute(password));

        return message;
    }

    /**
     * 生成 CHAP认证方式的 认证请求消息
     * @param userIP
     * @param serialNo
     * @param reqID
     * @param userName
     * @param chapPassword
     * @return
     */
    public static PortalMessage createCHAPAuthRequestMessage(UserIP userIP, int serialNo
            , int reqID, String userName, byte[] chapPassword) {
        PortalMessage message = new PortalMessage(Type.AuthReq, AuthType.CHAP, serialNo, reqID, userIP, ErrorCode.Default);
        message.addAttribute(PortalAttributeFacotry.createUsernameAttribute(userName));
        message.addAttribute(PortalAttributeFacotry.createChapPasswordAttribute(chapPassword));

        return message;
    }

    /**
     * 生成 认证应答 确认消息
     * @param authType
     * @param userIP
     * @param serialNo
     * @param reqID
     * @return
     */
    public static PortalMessage createAuthAckAffMessage(AuthType authType, UserIP userIP, int serialNo
            , int reqID) {

        return new PortalMessage(Type.AuthAckAff, authType, serialNo, reqID, userIP, ErrorCode.Default);
    }

    /**
     * 发送 用户下线请求消息
     * @param authType
     * @param userIP
     * @return
     */
    public static PortalMessage createLogoutRequestMessage(AuthType authType, UserIP userIP) {

        int serialNo = PortalUtil.getNextSerailNumber();

        return new PortalMessage(Type.LogoutReq, authType, serialNo, DEFAULT_REQ_ID, userIP, ErrorCode.Default);
    }

    /**
     * 生成 Challenge请求超时 消息
     * @param authType
     * @param serialNo
     * @param userIP
     * @return
     */
    public static PortalMessage createChallengeTimeoutMessage(AuthType authType, int serialNo, UserIP userIP) {
        return new PortalMessage(Type.LogoutReq, authType, serialNo, DEFAULT_REQ_ID, userIP, ErrorCode.Timeout);
    }

    /**
     * 生成 认证请求超时 消息
     * @param authType
     * @param serialNo
     * @param reqID
     * @param userIP
     * @return
     */
    public static PortalMessage createTimeoutMessage(AuthType authType, int serialNo, int reqID, UserIP userIP) {
        return new PortalMessage(Type.LogoutReq, authType, serialNo, reqID, userIP, ErrorCode.Timeout);
    }

}
