package com.digitalchina.dcn.dscc.portal.server;

import com.digitalchina.dcn.dscc.portal.protocol.PortalMessage;
import com.digitalchina.dcn.dscc.portal.protocol.PortalMessage.*;

/**
 * Created by Administrator on 2016/8/25 0025.
 */
public enum AuthStatus {

    Started(1), ChallengeReq(2), ChallengeAck(3),
    AuthReq(4), AuthAck(5), AuthAckAff(6), LogoutReq(7), LogoutAck(8);

    private final int status;

    AuthStatus(int status) {
        this.status = status;
    }

    public int getStatus() { return status; }

    /**
     * 根据portal message获得用户当前的认证状态
     * @param msg
     * @return
     */
    public static AuthStatus getStatusByPortalMessage(PortalMessage msg) {
        Type type = msg.type();

        switch (type) {
            case ChallengeReq: return ChallengeReq;
            case ChallengeAck: return ChallengeAck;
            case AuthReq: return AuthReq;
            case AuthAck: return AuthAck;
            case AuthAckAff: return AuthAckAff;
            case LogoutReq: return LogoutReq;
            case LogoutAck: return LogoutAck;
            default:
                throw new IllegalArgumentException("error message type: " + type);
        }


    }
}
