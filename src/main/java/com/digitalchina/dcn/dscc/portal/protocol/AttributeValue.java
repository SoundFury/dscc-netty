package com.digitalchina.dcn.dscc.portal.protocol;

import org.apache.spark.network.protocol.Encodable;

/**
 * Created by Administrator on 2016/8/25 0025.
 */
public interface AttributeValue extends Encodable{

    byte[] getBytes();

}
