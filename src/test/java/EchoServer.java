import io.netty.bootstrap.Bootstrap;
import io.netty.bootstrap.ServerBootstrap;
import io.netty.channel.Channel;
import io.netty.channel.ChannelFuture;
import io.netty.channel.ChannelInitializer;
import io.netty.channel.EventLoopGroup;
import io.netty.channel.nio.NioEventLoop;
import io.netty.channel.nio.NioEventLoopGroup;
import io.netty.channel.socket.nio.NioDatagramChannel;
import io.netty.channel.socket.nio.NioServerSocketChannel;

/**
 * Created by Administrator on 2016/8/20.
 */
public class EchoServer {
    private final int port;

    public EchoServer(int port) {
        this.port = port;
    }

    public void start() throws InterruptedException {
        EventLoopGroup group = new NioEventLoopGroup();

        try {
//            ServerBootstrap boot = new ServerBootstrap();
            Bootstrap boot = new Bootstrap();

            boot.group(group).channel(NioDatagramChannel.class)
                    .localAddress(port)
                    .handler(new ChannelInitializer<Channel>() {
                        @Override
                        protected void initChannel(Channel ch) throws Exception {
                            ch.pipeline().addLast("handler1", new EchoServerHandler())
                            .addLast("handler2", new EchoServerHandler());
                        }
                    });

            ChannelFuture future = boot.bind().sync();
            System.out.println(EchoServer.class.getName() + "started and listen on " + future.channel().localAddress()) ;
            future.channel().closeFuture().sync();
        } finally {

        }
    }

    public static void main(String[] args) throws InterruptedException {
        EchoServer server = new EchoServer(8023);
        server.start();
    }
}
